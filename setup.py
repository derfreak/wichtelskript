import setuptools


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

with open("requirements.txt", "r", encoding="utf-8") as fh:
    requirements = fh.readlines()

namespace = {}
init_path = setuptools.convert_path('src/WichtelSkript/__init__.py')
with open(init_path) as ver_file:
    exec(ver_file.read(), namespace)

setuptools.setup(
    name=namespace["__name__"],
    version=namespace["__version__"],
    author=namespace["__author__"],
    author_email="lukas.riedersberger@posteo.de",
    description="short description",
    url="https://gitlab.com/derfreak/wichtelskript",
    package_dir={'': 'src'},
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(where="src"),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.9',
    install_requires=requirements,
    include_package_data=True,
    entry_points={
      'console_scripts': [
          'wichtel = WichtelSkript.__main__:main'
      ]
    },
)
