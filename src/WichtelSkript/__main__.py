import sys
from numpy import random
import yaml
import logging

from WichtelSkript.mail import Wichtel


def main() -> int:
    if len(sys.argv) < 3:
        logging.error("The first argument must be a valid yml file containing email secrets\n"
                      "The second argument must be a valid yml file with all contestants (name: email).")
        return 1
    with open(sys.argv[2], "r", encoding='utf8') as file:
        try:
            users = yaml.safe_load(file)
        except yaml.YAMLError as exception:
            logging.error("Could not parse yaml file: %s", str(exception))
            return 1

    users = list(users.items())
    random.shuffle(users)

    try:
        wichtel = Wichtel(
            email_config=sys.argv[1]
        )
    except RuntimeError as exception:
        logging.error("Failed to create the Wichtel: %s", str(exception))
        return 1

    for i in range(0, len(users)):
        wichtel.send_mail(
            gifter=users[i],
            gifted=users[(i + 1) % len(users)],
        )

    return 0


if __name__ == "__main__":
    sys.exit(main())
