import smtplib
import ssl
import yaml
from datetime import datetime
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import WichtelSkript


class Wichtel:

    def __init__(self, email_config: str):
        with open(email_config, "r", encoding='utf8') as file:
            try:
                self.email_config = yaml.safe_load(file)
            except yaml.YAMLError as exception:
                raise RuntimeError(f"Could not parse yaml file: {exception}")

        self.sender_name = self.email_config['name']
        self.sender_mail = self.email_config['mail']

    def send_mail(self, gifter: str, gifted: str):
        recipient_name, recipient_email = gifter
        gifted_name, _ = gifted

        name = recipient_name.split()[0]

        text = f"""\
        Hohoho {name} :)

        Hier spricht das Wichtelprogramm.

        Dieses Jahr beschenkst du

        '{gifted_name}'

        mit einer Kleinigkeit bis 10€. Aber niemandem verraten - Streng geheim !

        Eine schöne Adventszeit,
        dein Wichtelprogramm

        ____________________
        Author: {WichtelSkript.__author__}
        Version: {WichtelSkript.__version__}
        """

        html = f"""\
        <html>
          <head></head>
          <body>
            <h4>Hohoho {name} :)</h4>

            Hier spricht das Wichtelprogramm.<br><br>

            Dieses Jahr beschenkst du<br><br>

            <strong><big><big><big>{gifted_name}</big></big></big></strong>

            <br><br>mit einer Kleinigkeit bis 10€. Aber niemandem verraten - <strong>Streng geheim !</strong>

            <br><br>Eine schöne Adventszeit, <br>dein Wichtelprogramm.<br>
            ____________________<br>
            Author: {WichtelSkript.__author__}<br>
            Version: {WichtelSkript.__version__}<br>
          </body>
        </html>
        """

        message = MIMEMultipart("alternative")
        message["Subject"] = f"🎄 Wichteln {datetime.now().year} 🎄 - Darf nur {name} lesen"
        message["From"] = self.sender_mail
        message["To"] = recipient_email
        message.attach(MIMEText(text, "plain"))
        message.attach(MIMEText(html, "html"))

        context = ssl.create_default_context()

        with smtplib.SMTP_SSL(
                host=self.email_config['host'],
                port=self.email_config['port'],
                context=context,
        ) as server:
            server.login(
                user=self.sender_mail,
                password=self.email_config['password']
            )
            server.sendmail(
                self.sender_mail,
                recipient_email,
                message.as_string()
            )
